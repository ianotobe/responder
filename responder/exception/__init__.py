class InvalidPayloadException(Exception):
    pass

class InvalidSchemaException(Exception):
    pass
