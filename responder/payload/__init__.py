from datetime import datetime
from uuid import uuid4

from responder.exception import InvalidSchemaException, InvalidPayloadException
from responder.payload.linked_node import LinkedNode

types = ["str", "int", "float", "bool", "datetime", "schema"]

class PayloadSchema:

    def __init__(self, schema):
        self._validate_schema(schema)
        for key, node in self._schema_generator(schema):
            setattr(self, key, node)

    def _schema_generator(self, schema):
        for key, val in schema.items():
            if val["type"] == "schema":
                node = LinkedNode(
                    val["required"],
                    val["type"],
                    schema=self._schema_generator(val["schema"])
                )
            elif val["type"] == "datetime":
                node = LinkedNode(
                    val["required"],
                    val["type"],
                    format=val["format"]
                )
            else:
                node = LinkedNode(
                    val["required"],
                    val["type"]
                )
            yield key, node

    def _validate_schema(self, schema):

        failure = InvalidSchemaException("Schema validation failed. Double check format.")

        for key, val in schema.items():
            if not isinstance(val, dict):
                raise failure

            required = val.get("required")
            if required == None or not isinstance(required, bool):
                raise failure

            value_type = val.get("type")
            if value_type == None or value_type not in types:
                raise failure
            elif value_type == "datetime":
                format = val.get("format")
                if format == None:
                    raise failure
                else:
                    try:
                        datetime.now().strftime(format)
                    except:
                        raise failure
            elif value_type == "schema":
                sub_schema = val.get("schema")
                if sub_schema == None:
                    raise failure
                else:
                    self._validate_schema(sub_schema)


class Payload:

    def __init__(self, schema):
        if isinstance(schema, PayloadSchema):
            self.schema = schema
        elif isinstance(schema, dict):
            self.schema = PayloadSchema(schema)
        self.type_dict = {
            "str":str,
            "int":int,
            "float":float,
            "bool":bool,
            "schema":dict
        }

    def format(self, payload):
        self._validate_payload(self.schema.__dict__, payload)
        return payload

    def _validate_payload(self, schema, payload):
        schema_keys = list(schema.keys())
        payload_keys = list(payload.keys())
        if not all([key in schema_keys for key in payload_keys]):
            raise Exception("An unspecified field was provided in payload")
        for key in schema_keys:
            schema_node = schema[key]
            payload_node = payload.get(key)
            if payload_node == None:
                if schema_node.required == True:
                    raise Exception(f"Required field '{key}' is pissing from payload")
            else:
                if schema_node.value_type == "datetime":
                    try:
                        payload[key] = payload[key].strftime(schema_node.format)
                    except:
                        raise Exception(f"Could not format datetime object '{key}' according to specification")
                else:
                    try:
                        payload[key] = self.type_dict[schema_node.value_type](payload[key])
                    except:
                        raise Exception(f"Could not cast {key} as a {schema_node.value_type}")
                if schema_node.schema:
                    self._validate_payload(schema_node.schema, payload[key])
