class LinkedNode:

    def __init__(self, required, value_type, format=None, schema=None):
        self.required = required
        self.value_type = value_type
        self.format = format
        self.schema = schema
        if self.schema:
            self.schema = {key:val for key, val in schema}
