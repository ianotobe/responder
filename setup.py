import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

def requirements():
    requires = []
    with open("requirements.txt", "r") as req_file:
        for line in req_file.readlines():
            requires.append(line.strip())
    return requires

setuptools.setup(
    name="responder",
    version="0.0.0",
    author="Ian Otobe",
    author_email="ianotobe@example.com",
    description="A simple way to manage API responses",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ianotobe/responder",
    python_requires='>=3.6',
    install_requires=requirements()
)
